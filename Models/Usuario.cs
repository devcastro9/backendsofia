﻿using Microsoft.AspNetCore.Identity;

namespace BackendSofia.Models
{
    public class Usuario : IdentityUser
    {
        public int DepartamentoId { get; set; }
    }
}
