# Backend del sistema Sofia
Backend del sistema SOFIA:

    1. BackEnd:

        * Lenguaje de programacion backend: C# 11

        * SDK: .NET7 v7 (version LTS vigente hasta el 14 de mayo de 2024)

        * ORM: Entity Framework Core v7 (Vigente hasta el 14 de mayo de 2024)

        * Base de datos: Postgresql v15

    2. FrontEnd

        * Manejador de versiones de NodeJS: NVM

        * NodeJS v18.14.0 (Version LTS vigente hasta el 23 de septiembre de 2023)

        * NPM v9.3.1
        
        * Angular CLI: ng v15.1.4

        * Angular v15.1.4

        * Angular Material v15.1.4

        * Librerias Adicionales: ChartJS v3.8.0 (ng2-charts), IntroJS v5.1.0 (angular-intro.js), SweetAlert2 v11.4.20 (ngx-sweetalert2)

    3. Infraestructura

        * Windows Server 2019/Ubuntu 20 (Sistema operativo del Servidor)

        * Servidor web: IIS v10 (Servidor web con proxy inverso, en caso de despliegue con Docker)

        * Docker v20 (Contenedores)


## Comando de ingenieria inversa:
```
Scaffold-DbContext "Server=3.83.63.203;Port=5432;Database=admin_empresa;User Id=postgres;Password=Edificio123*;" -Provider Npgsql.EntityFrameworkCore.PostgreSQL -DataAnnotations -Context AppDbContext -ContextDir Data -OutputDir Models -NoOnConfiguring -Force
```
